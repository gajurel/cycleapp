﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using ZedGraph;

namespace Cycle
{
    public partial class Data_graph : Form
    {
        private int tick;
        int ticktimes = 0;
        
        DataTable viewClients= new DataTable("File");
        int rowCount = 0;

        public Data_graph()
        {
            InitializeComponent();
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int repeats = 0;
            DataTable list = new DataTable();
            string line;
            StreamReader read = new StreamReader("File.txt");
            while ((line = read.ReadLine()) != null)
            {
                if (repeats == 0)
                {
                    string[] words = line.Split('\t');
                    foreach (string word in words)
                    {
                        viewClients.Columns.Add(word);
                    }
                }

                if (repeats > 0)
                {

                    string[] words = line.Split('\t');
                    viewClients.Rows.Add(words[0], words[1], words[2], words[3], words[4], words[5]);
                }
                repeats++;
            }
            rowCount = viewClients.Rows.Count;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           
            
            if (ticktimes >= 0)
            {
               string hrVal =  viewClients.Rows[ticktimes][0].ToString();
               string spdVal = viewClients.Rows[ticktimes][1].ToString();
               string cadVal = viewClients.Rows[ticktimes][2].ToString();
               string altVal = viewClients.Rows[ticktimes][3].ToString();
               string pwrVal = viewClients.Rows[ticktimes][4].ToString();
               string balVal = viewClients.Rows[ticktimes][5].ToString();
               double Hr= Convert.ToDouble(hrVal);
               double Spd = Convert.ToDouble(spdVal);
               double Cad = Convert.ToDouble(cadVal);
               double Alt = Convert.ToDouble(altVal);
               double Pwr = Convert.ToDouble(pwrVal);
               double Bal = Convert.ToDouble(balVal);

               hrValue.Text = hrVal;
               spdValue.Text = spdVal;
               cadValue.Text = cadVal;
               altValue.Text = altVal;
               pwrValue.Text = pwrVal;
               balValue.Text = balVal;
                 
                draw(Hr,Spd,Cad,Alt,Pwr,Bal);
            }

        }

            private void draw(double hr, double spd, double cad, double alt, double pwr, double bal)
            {
               
                if (zedGraphControl1.GraphPane.CurveList.Count <= 0)
                    return;
                LineItem curve1 = zedGraphControl1.GraphPane.CurveList[0] as LineItem;
                LineItem curve2 = zedGraphControl1.GraphPane.CurveList[1] as LineItem;
                LineItem curve3 = zedGraphControl1.GraphPane.CurveList[2] as LineItem;
                LineItem curve4 = zedGraphControl1.GraphPane.CurveList[3] as LineItem;
                LineItem curve5 = zedGraphControl1.GraphPane.CurveList[4] as LineItem;
                
                if (curve1 == null)
                    return;
                if (curve2 == null)
                    return;
                if (curve3 == null)
                    return;
                if (curve4 == null)
                    return; 
                if (curve5 == null)
                    return;
                
                IPointListEdit list1 = curve1.Points as IPointListEdit;
                IPointListEdit list2 = curve2.Points as IPointListEdit;
                IPointListEdit list3 = curve3.Points as IPointListEdit;
                IPointListEdit list4 = curve4.Points as IPointListEdit;
                IPointListEdit list5 = curve5.Points as IPointListEdit;
                

                if (list1 == null)
                    return;
                if (list2 == null)
                    return;
                if (list3 == null)
                    return;
                if (list4 == null)
                    return;
                if (list5 == null)
                    return;
                

                double time = (Environment.TickCount) / 1000.0;


                list1.Add(time, hr);
                list2.Add(time, spd);
                list3.Add(time, cad);
                list4.Add(time, alt);
                list5.Add(time, pwr);
                
                Scale xscale = zedGraphControl1.GraphPane.XAxis.Scale;
                if(time>xscale.Max-xscale.MajorStep)
                {
                    xscale.Max = time + xscale.MajorStep;
                    xscale.Min = xscale.Max - 30.0;
                }
                zedGraphControl1.AxisChange();
                zedGraphControl1.Invalidate();

                ticktimes++;
          
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if(btnStart.Text=="Start")
            {
                btnStart.Text = "Stop";
                timer1.Start();
                GraphPane pane = zedGraphControl1.GraphPane;
                //pane.Title = "USA Client Work";
                //gPane.XAxis.Title = "Position";
                //gPane.YAxis.Title = "Level";

                RollingPointPairList list1 = new RollingPointPairList(60000);
                RollingPointPairList list2 = new RollingPointPairList(60000);
                RollingPointPairList list3 = new RollingPointPairList(60000);
                RollingPointPairList list4 = new RollingPointPairList(60000);
                RollingPointPairList list5 = new RollingPointPairList(60000);
                RollingPointPairList list6 = new RollingPointPairList(60000);

                LineItem hr = pane.AddCurve("HR", list1, Color.Red, SymbolType.Square);
                LineItem spd = pane.AddCurve("SPD", list2, Color.Blue, SymbolType.Square);
                LineItem cad = pane.AddCurve("CAD", list3, Color.Green, SymbolType.Square);
                LineItem alt = pane.AddCurve("ALT", list4, Color.Yellow, SymbolType.Square);
                LineItem pwr = pane.AddCurve("PWR", list5, Color.Orange, SymbolType.Square);
               

                pane.XAxis.IsVisible = true;
                pane.YAxis.Scale.Min = 80;
                pane.YAxis.Scale.Max = 1000;
                pane.XAxis.Scale.Min = 0;
                pane.XAxis.Scale.Max = 8000;
                pane.XAxis.Scale.MinorStep = 5;
                pane.XAxis.Scale.MajorStep = 5;
                zedGraphControl1.AxisChange();

                tick = Environment.TickCount;
            }
            else
            {
                timer1.Stop();
                btnStart.Text = "Start";
            }
           
            

            //int counter = 0;
            //            this.Text = counter.ToString();

            //            string line;
            //            StreamReader read = new StreamReader("USAClientWork.txt");
            //            while ((line = read.ReadLine()) != null)
            //            {
            //                if (counter == 0)
            //                {
            //                    string[] words = line.Split('\t');
            //                    foreach (string word in words)
            //                    {

            //                    }
            //                }

            //                if (counter > 0)
            //                {

            //                    PointPairList list1 = new PointPairList();
            //                    PointPairList list2 = new PointPairList();
            //                    string[] words = line.Split('\t');
            //                    for (int i = 0; i < 6; i++)
            //                    {
            //                        double x, y1;

            //                        string xvalue = words[i];
            //                        x = (double)i + 5;
            //                        y1 = Convert.ToDouble(xvalue);
            //                        list1.Add(x, y1);

            //                    }
            //                    LineItem myCurve = pane.AddCurve("", list1, Color.Blue, SymbolType.Diamond);
            //                    zedGraphControl1.AxisChange();
            //                    timer1.Stop();
            //                    timer1.Start();
            //                }

            //                counter++;

            //            }
        }

        private void viewHRMFileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void hRDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main hrd = new Main();
            hrd.Show();
            this.Hide();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

 
    }

    }

